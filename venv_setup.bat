cls

:: =================================================
:: Deactivate current Virtual Environment
:: =================================================

@echo off

set VIRTUAL_ENV=

REM Don't use () to avoid problems with them in %PATH%
if not defined _OLD_VIRTUAL_PROMPT goto ENDIFVPROMPT
    set "PROMPT=%_OLD_VIRTUAL_PROMPT%"
    set _OLD_VIRTUAL_PROMPT=
:ENDIFVPROMPT

if not defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVHOME
    set "PYTHONHOME=%_OLD_VIRTUAL_PYTHONHOME%"
    set _OLD_VIRTUAL_PYTHONHOME=
:ENDIFVHOME

if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH
    set "PATH=%_OLD_VIRTUAL_PATH%"
    set _OLD_VIRTUAL_PATH=
:ENDIFVPATH

@echo on

:: =================================================
:: Remove old Virtual Environment
:: =================================================

DEL /F/Q/S "venv" > NUL
RMDIR /Q/S "venv"

:: =================================================
:: Create new Virtual Environment
:: =================================================

mkdir venv
cd venv
pip install virtualenv
virtualenv project

:: =================================================
:: Activate new Virtual Environment
:: =================================================

@echo off

set "VIRTUAL_ENV=C:\Users\leenr\Documents\PhD\software repositories\log-polar-cnn\venv\project

if defined _OLD_VIRTUAL_PROMPT (
    set "PROMPT=%_OLD_VIRTUAL_PROMPT%"
) else (
    if not defined PROMPT (
        set "PROMPT=$P$G"
    )
    if not defined VIRTUAL_ENV_DISABLE_PROMPT (
        set "_OLD_VIRTUAL_PROMPT=%PROMPT%"
    )
)
if not defined VIRTUAL_ENV_DISABLE_PROMPT (
    set "PROMPT=(project) %PROMPT%"
)

REM Don't use () to avoid problems with them in %PATH%
if defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVHOME
    set "_OLD_VIRTUAL_PYTHONHOME=%PYTHONHOME%"
:ENDIFVHOME

set PYTHONHOME=

REM if defined _OLD_VIRTUAL_PATH (
if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH1
    set "PATH=%_OLD_VIRTUAL_PATH%"
:ENDIFVPATH1
REM ) else (
if defined _OLD_VIRTUAL_PATH goto ENDIFVPATH2
    set "_OLD_VIRTUAL_PATH=%PATH%"
:ENDIFVPATH2

set "PATH=%VIRTUAL_ENV%\Scripts;%PATH%"

@echo on

:: =================================================
:: Install Python Packages and Dependencies
:: =================================================

python -m pip install --upgrade pip
pip install theano==1.0.4
pip install keras==2.2.5
pip install opencv-python==4.1.1.26
pip install pandas==0.25.1
pip install Pillow==6.2.1
pip install pytesseract==0.3.0
pip install numpy==1.16.4
pip install imutils==0.5.3
pip install matplotlib==3.1.1
pip install pydot==1.4.1


cd ../

set "KERAS_BACKEND=theano"

pip freeze

:: =================================================
