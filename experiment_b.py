import lib_logpolar

# ==============================================================================

rotation = 0
scale = 1

# Mode
mode = "load" # train / load

# ==============================================================================

# Load MNIST data
x_train, y_train, x_test, y_test = lib_logpolar.load_data(rotation, scale, log_polar_transform=False)
dataset = (x_train, y_train, x_test, y_test)

lib_logpolar.show_image(x_train[0], "Training Set: Image #1")
lib_logpolar.show_image(x_test[0], "Testing Set: Image #1")

# ==============================================================================

# Initialize NN
model = lib_logpolar.init_nn()
model.summary()

# Training Epochs
epochs = 5

# ==============================================================================

# Train NN
if (mode == "train"):

    print ("\nTraining NN...\n")
    model = lib_logpolar.train_nn(model, epochs, dataset)
    lib_logpolar.save_nn_to_file(model, filename="euclidean.h5")
    print ("Done")

if (mode == "load"):

    print ("\nLoading NN from file...\n")
    model = lib_logpolar.load_nn_from_file(filename="euclidean.h5")
    print ("Done")

# ==============================================================================

# Test NN (benchmarking)
print ("\nTesting NN with testing set...\n")

for scale_i in [1,0.9,0.8,0.7,0.6,0.5,0.4,0.3]:
    print ("\nScale: %.1f" % scale_i)
    for rotation_i in [0,45,90,135,180,225,270,315]:
        print ("\nRotation: %d" % rotation_i)
        x_train, y_train, x_test, y_test = lib_logpolar.load_data(rotation_i, scale_i, log_polar_transform=False)
        dataset = (x_train, y_train, x_test, y_test)
        lib_logpolar.show_image(x_test[0], title="Test Image #1: Scale: %.1f, Rotation: %d" % (scale_i, rotation_i))
        test_loss, test_acc = lib_logpolar.test_nn(model, dataset)
        print ("test_acc: %.3f" % test_acc)

# ==============================================================================

# Run
filesize_list = lib_logpolar.calculate_filesize(x_test)

import pdb; pdb.set_trace()
