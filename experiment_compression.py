import lib_logpolar
import numpy as np

# ==============================================================================

rotation = 0
scale = 1
epochs = 5

# ==============================================================================

# Run
print ("Compression Simulation: ")
for i in [7.5,10,15,20]:

    # Initialize NN
    model = lib_logpolar.init_nn()

    # Generate datasets
    x_train, y_train, x_test, y_test = lib_logpolar.load_data(rotation, scale, log_polar_transform=True, log_polar_res=i)
    dataset = (x_train, y_train, x_test, y_test)

    # Calculate filesize
    filesize_list = lib_logpolar.calculate_filesize(x_test, i)
    average = np.average(filesize_list)
    std = np.std(filesize_list)

    # Train NN
    model = lib_logpolar.train_nn(model, epochs, dataset)
    lib_logpolar.save_nn_to_file(model, filename="logpolar_%02d.h5" % (i))

    # Test NN
    test_loss, test_acc = lib_logpolar.test_nn(model, dataset)

    print ("log_polar_res: %d, compression: %2.4f %%, test_acc: %2.4f" % (i, average, test_acc))

import pdb; pdb.set_trace()
