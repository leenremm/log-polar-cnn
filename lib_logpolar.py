#!/usr/bin/python
import os
import cv2
from PIL import Image
import numpy as np
np.random.seed(0)
import warnings
warnings.simplefilter(action='ignore')
os.environ["KERAS_BACKEND"] = "theano"
from keras.layers import Input, Dense, Flatten, MaxPooling2D, Conv2D, BatchNormalization, Dropout
from keras.models import Sequential, Model, load_model
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.datasets import mnist
from keras.utils import plot_model
import matplotlib.pyplot as plt

# ==============================================================================

def load_data(rotation=0, scale=1, log_polar_transform=False, log_polar_res=5.0):

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # Normalize all values between 0 and 1
    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # Distort Testing Set only
    x_train, x_train_rotation, x_train_scale = distort_dataset(x_train, 0, 1, log_polar_transform=log_polar_transform, log_polar_res=log_polar_res)
    x_test, x_test_rotation, x_test_scale = distort_dataset(x_test, rotation, scale, log_polar_transform=log_polar_transform, log_polar_res=log_polar_res)

    # Flatten the 28x28 images into vectors of size 784
    x_train = x_train.reshape((len(x_train), 28, 28, 1))
    x_test = x_test.reshape((len(x_test), 28, 28, 1))

    return x_train, y_train, x_test, y_test

# ==============================================================================

def filesize(img):

    w, h = img.shape
    max_size = w * h
    blank_pixels = 0
    for y in range(0,h):
        column = [i[y] for i in img]
        if (sum(column) == 0):
            blank_pixels += h
        else:
            blank_pixels = 0
    used_pixels = max_size - blank_pixels
    return used_pixels, max_size

# ==============================================================================

def init_nn(input_dim=(28, 28, 1), output_dim=10):

    '''
    # https://medium.com/@mrkardostamas/emnist-handwritten-character-recognition-with-deep-learning-b5d61ac1aab7
    model = Sequential()
    model.add(Conv2D(32, kernel_size = 3, activation='relu', input_shape = input_dim))
    model.add(BatchNormalization())
    model.add(Conv2D(32, kernel_size = 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, kernel_size = 5, strides=2, padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))

    model.add(Conv2D(64, kernel_size = 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size = 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size = 5, strides=2, padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))

    model.add(Conv2D(128, kernel_size = 4, activation='relu'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.4))
    model.add(Dense(output_dim, activation='softmax'))
    '''

    # https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_dim))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(output_dim, activation='softmax'))

    # Compile the model
    model.compile(
      optimizer='adam',
      loss='categorical_crossentropy',
      metrics=['accuracy'],
    )

    return model

# ==============================================================================

def train_nn(model, epochs, dataset):

    train_images, train_labels, test_images, test_labels = dataset

    # Train the model
    epochs = epochs
    history = model.fit(
      train_images,
      to_categorical(train_labels),
      epochs=epochs,
      batch_size=256,
    )

    '''
    plt.plot(history.history['acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy (0-100%)')
    plt.xlabel('epoch')
    plt.legend(['accuracy'], loc='bottom right')
    plt.show()
    '''

    return model

# ==============================================================================

def test_nn(model, dataset):

    train_images, train_labels, test_images, test_labels = dataset

    # Test the model
    test_loss, test_acc = model.evaluate(test_images, to_categorical(test_labels))

    return test_loss, test_acc

# ==============================================================================

def calculate_filesize(dataset, log_polar_res=5.0):

    filesize_list = []
    for i in range (len(dataset)):

        img = dataset[i].reshape((28, 28)).astype(np.float32)
        used_pixels, max_size = filesize(img)
        filesize_list.append(used_pixels/max_size)

    return filesize_list

# ==============================================================================

def rotate_resize(img, rotate_degrees=0, resize_percent=1, background_colour=0):

    # Original size
    wo, ho = img.shape

    # Convert to Pillow format
    image_obj = Image.fromarray(img)

    # Resize
    w1 = int(image_obj.size[0] * resize_percent)
    h1 = int(image_obj.size[1] * resize_percent)
    resized_image = image_obj.resize((w1, h1), Image.NEAREST)

    # Rotate
    rotated_image = resized_image.rotate(rotate_degrees)

    # Overlay on black background
    offset = ( int((wo - w1)/2), int((ho - h1)/2) )
    black_background = Image.new('F', (wo, ho), background_colour)
    black_background.paste(rotated_image, offset)

    return np.asarray(black_background)

# ==============================================================================

def distort_dataset(data, rotate_degrees=0, resize_percent=1, log_polar_transform=False, log_polar_res=5.0):

    x_data = []
    x_logpolar = []
    x_logpolar_inv = []
    x_rotation = []
    x_scale = []
    for i in range(0, len(data)):

        # Rotate & Resize
        i_data = rotate_resize(data[i], rotate_degrees, resize_percent)

        # Log Polar Transform
        if (log_polar_transform == True):
            i_data = transform_logpolar(i_data, log_polar_res=log_polar_res)

        # Append to output lists
        x_data.append(i_data)
        x_rotation.append(rotate_degrees)
        x_scale.append(resize_percent)

    return np.array(x_data), np.array(x_rotation), np.array(x_scale)

# ==============================================================================

def transform_logpolar(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    #print ("cv2.logPolar Value: %s" % value)
    log_polar_image = cv2.logPolar(img,(img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS)
    return log_polar_image

# ==============================================================================

def transform_logpolar_inv(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    inv_log_polar_image = cv2.logPolar(img, (img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS + cv2.WARP_INVERSE_MAP)
    return inv_log_polar_image

# ==============================================================================

def save_img_to_file(img, save_location):

    img.save(saved_location)

# ==============================================================================

def show_image(img, title="Image"):

    img = cv2.resize(img, (280,280), interpolation = cv2.INTER_AREA)
    cv2.namedWindow(title)        # Create a named window
    cv2.moveWindow(title, 40,30)  # Move it to (40,30)
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# ==============================================================================

def save_nn_to_file(model, filename="model.h5", path="saved_nn_models"):

    # Save model and weights
    save_dir = os.path.join(os.getcwd(), path)
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    model_path = os.path.join(path, filename)
    model.save(model_path)

def load_nn_from_file(filename="model.h5", path="saved_nn_models"):

    save_dir = os.path.join(os.getcwd(), path)
    model_path = os.path.join(save_dir, filename)
    model = load_model(model_path)

    return model

# ==============================================================================
