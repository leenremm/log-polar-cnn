# Rotation and Scale Invariance: Log-polar pre-processing for Convolutional Neural Networks

## About

Author: Leendert A Remmelzwaal

Written: October 2019

License: CC BY NC

## Description

A bio-inspired pre-processing step using Log-polar transformations to create a Rotation and Scale Invariant Convolutional Neural Network.

## Software Installation

1) Install Python 3.6.4 or later: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

2) Install Miniconda (this is to make use of highly optimized C++ implementation of NN training): https://docs.conda.io/en/latest/miniconda.html

## Virtual Environment Setup

3) Open Miniconda command prompt.

5) Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies

## Running the code

1) Open a Miniconda command prompt (only required for running optimized NN training, otherwise can use normal CMD).
2) Navigate to the directory containing main.py
3) Run the following in the Windows command line:

```
venv_activate.bat
cls && python experiment_a.py
cls && python experiment_b.py
cls && python experiment_c.py
venv_deactivate.bat
```
