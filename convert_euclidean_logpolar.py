import lib_logpolar
from PIL import Image
import numpy as np
import cv2

# ==============================================================================

log_polar_res = 50
image_path = "img\\apple_center.png"
image_path = "img\\apple_bottom.png"
image_path = "img\\thank_you_2.jpg"

# ==============================================================================

# Open Image
image_obj = cv2.imread(image_path)

# Show Original
lib_logpolar.show_image(image_obj, ("Original Image: %s" % image_path))

# Log Polar format
logpolar = lib_logpolar.transform_logpolar(image_obj, log_polar_res=log_polar_res)
lib_logpolar.show_image(logpolar, ("Log Polar Format: %s" % image_path))

# Inverse Log Polar format
reconstruct = lib_logpolar.transform_logpolar_inv(logpolar, log_polar_res=log_polar_res)
lib_logpolar.show_image(reconstruct, ("Inverse Log Polar (reconstruction): %s" % image_path))

import pdb; pdb.set_trace()
