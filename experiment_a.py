import lib_logpolar

# ==============================================================================

rotation = 0
scale = 1

# Mode
mode = "load" # train / load

# ==============================================================================

# Load MNIST data
x_train, y_train, x_test, y_test = lib_logpolar.load_data(rotation, scale, log_polar_transform=False)
dataset = (x_train, y_train, x_test, y_test)

lib_logpolar.show_image(x_train[0], "Training Set: Image #1")
lib_logpolar.show_image(x_test[0], "Testing Set: Image #1")

# ==============================================================================

# Initialize NN
model = lib_logpolar.init_nn()
model.summary()

# Training Epochs
epochs = 5

# ==============================================================================

# Train NN
if (mode == "train"):

    print ("\nTraining NN...\n")
    model = lib_logpolar.train_nn(model, epochs, dataset)
    lib_logpolar.save_nn_to_file(model, filename="euclidean.h5")
    print ("Done")

if (mode == "load"):

    print ("\nLoading NN from file...\n")
    model = lib_logpolar.load_nn_from_file(filename="euclidean.h5")
    print ("Done")

# ==============================================================================

# Test NN (benchmarking)
print ("\nTesting NN with testing set...\n")
test_loss, test_acc = lib_logpolar.test_nn(model, dataset)

# ==============================================================================

# Run
filesize_list = lib_logpolar.calculate_filesize(x_test)

import pdb; pdb.set_trace()
